$(function () {
		  $("[data-toggle='tooltip']").tooltip();
		  $("[data-toggle='popover']").popover();
		  $('.carousel').carousel({
		  	interval:5000
		  });

		  $('#encarga').on('show.bs.modal', function (e) {
  				console.log("el modal se esta mostrando");

  				$('#encargaBtn').removeClass('btn-outline-success');
  				$('#encargaBtn').addClass('btn-comprar');
  				$('#encargaBtn').prop('disabled',true);
 			});

		  $('#encarga').on('shown.bs.modal', function (e) {
  				console.log("el modal se mostro");
 			});

		  $('#encarga').on('hide.bs.modal', function (e) {
  				console.log("el modal contacto se oculta");
 			});

		  $('#encarga').on('hidden.bs.modal', function (e) {
  				console.log("el modal contacto se oculto");
  				$('#encargaBtn').prop('disabled',false);
 			});
		});